import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  bg_en: string;
  bg_in: string;

  styleExp = 'red';

  constructor(
    public appComponent: AppComponent,
    public storage: Storage,
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.storage.get('bahasa').then((val) => {
      if (val == 'en' || val == null) {
        this.bg_en = '#70a1ff';
        this.bg_in = '';
      } else {
        this.bg_en = '';
        this.bg_in = '#70a1ff';
      }
    });
  }

  bahasa(val) {
    this.appComponent.pubBahasa({
      bahasa: val
    });

    if (val == 'en') {
      this.bg_en = '#70a1ff';
      this.bg_in = '';
    } else {
      this.bg_en = '';
      this.bg_in = '#70a1ff';
    }
  }

}
