import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { HomePage } from './home';
// import { HomeFilterPage } from '../home-filter/home-filter';
import { HomePageRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    TranslateModule.forChild(),
  ],
  declarations: [
    HomePage,
    // HomeFilterPage
  ],
  entryComponents: [
    // HomeFilterPage
  ],
  exports: [
    HomePage
  ]
})
export class HomeModule { }
