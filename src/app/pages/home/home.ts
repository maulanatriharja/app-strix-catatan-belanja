import { AlertController, ModalController, Platform, ToastController } from '@ionic/angular';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free/ngx';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  styleUrls: ['./home.scss'],
})
export class HomePage implements OnInit {

  loading: boolean = true;
  data: any = [];

  constructor(
    public admobFree: AdMobFree,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    public platform: Platform,
    public router: Router,
    public sqlite: SQLite,
    public storage: Storage,
    public toastController: ToastController,
    public translate: TranslateService,
  ) {
  }

  ngOnInit() {
    // this.platform.backButton.subscribe(async () => {
    //   const alert = await this.alertController.create({
    //     header: 'Konfirmasi',
    //     message: 'Keluar dari aplikasi ?',
    //     buttons: [
    //       {
    //         text: 'Batal',
    //         role: 'cancel',
    //         cssClass: 'secondary',
    //         handler: (blah) => {
    //           console.log('Confirm Cancel: blah');
    //         }
    //       }, {
    //         text: 'Keluar',
    //         handler: () => {
    //           console.log('Confirm Okay');
    //           navigator['app'].exitApp();
    //         }
    //       }
    //     ]
    //   });

    //   await alert.present();
    // });
  }

  ionViewWillEnter() {

  }

  ionViewDidEnter() {
    this.platform.ready().then(() => {
      this.load_data();
      this.load_admob();
    });
  }

  load_admob() {
    const bannerConfig: AdMobFreeBannerConfig = {
      isTesting: false,
      id: 'ca-app-pub-3302992149561172/4289436827',
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);

    this.admobFree.banner.prepare().then(() => {
    }).catch(e => console.log(e));
  }

  load_data() {
    this.loading = true;
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'SELECT * FROM grup ORDER BY tanggal DESC';
      db.executeSql(query, []).then((data) => {
        this.data = [];

        if (data.rows.length > 0) {
          for (let i = 0; i < data.rows.length; i++) {
            this.data.push(data.rows.item(i));
          }

          this.loading = false;
        } else {
          this.loading = false;
        }

        console.info(this.data);
      }).catch(e => {
        console.error(e);
        this.loading = false;
        // alert(JSON.stringify(e.code));
      });
    }).catch(e => console.error(e));
  }
}
