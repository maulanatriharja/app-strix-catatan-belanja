import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { AdMobFree } from '@ionic-native/admob-free/ngx';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';

import * as moment from 'moment';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  id_grup: any;

  judul: string = '';
  tanggal: any;


  loading: boolean = true;
  // empty: boolean = true;

  data: any = [];
  total: number = 0;

  pop_judul_tambah: string;
  pop_judul_update: string;
  pop_nama: string;
  pop_harga: string;
  pop_kuantiti: string;
  pop_batal: string;
  pop_tambah: string;
  pop_tambah_sukses: string;
  pop_update: string;
  pop_update_sukses: string;
  pop_hapus: string;
  pop_hapus_sukses: string;

  showFooter: boolean = true;

  constructor(
    public admobFree: AdMobFree,
    public alertController: AlertController,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public platform: Platform,
    public route: ActivatedRoute,
    public router: Router,
    public sqlite: SQLite,
    public storage: Storage,
    public toastController: ToastController,
    public translate: TranslateService,
  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('id_grup') == '') {
      this.id_grup = moment().format('YYYYMMDDHHmmss');
      this.tanggal = moment().format('YYYY-MM-DD')
    } else {
      this.id_grup = this.route.snapshot.paramMap.get('id_grup');
    }

    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query = 'CREATE TABLE IF NOT EXISTS list ( id INTEGER PRIMARY KEY, id_grup, nama_item, harga, kuantiti, st_check)';
        db.executeSql(query, []).then(() => console.log('Success execute SQL')).catch(e => console.log(e));

        let query_grup = 'CREATE TABLE IF NOT EXISTS grup ( id INTEGER PRIMARY KEY, id_grup, judul, tanggal, total)';
        db.executeSql(query_grup, []).then(() => console.log('Success execute SQL')).catch(e => console.log(e));
      }).catch(e => console.log(e));
    });
  }

  ionViewWillEnter() {
    this.storage.get('bahasa').then((val) => {
      if (val == 'en' || val == null) {
        // this.bahasa = val

        this.pop_judul_tambah = 'Add item';
        this.pop_judul_update = 'Update item';
        this.pop_nama = 'Item Name';
        this.pop_harga = 'Price';
        this.pop_kuantiti = 'Qty';
        this.pop_batal = 'Cancel';
        this.pop_tambah = 'Add';
        this.pop_tambah_sukses = 'Item has been added'
        this.pop_update = 'Update';
        this.pop_update_sukses = 'Item has been updated'
        this.pop_hapus = 'Delete';
        this.pop_hapus_sukses = 'Item has been deleted'
      } else {
        // this.bahasa = val

        this.pop_judul_tambah = 'Tambah barang';
        this.pop_judul_update = 'Update barang';
        this.pop_nama = 'Nama barang';
        this.pop_harga = 'Harga';
        this.pop_kuantiti = 'Kuantiti';
        this.pop_batal = 'Batal';
        this.pop_tambah = 'Tambah';
        this.pop_tambah_sukses = 'Barang telah ditambahkan'
        this.pop_update = 'Update';
        this.pop_update_sukses = 'Barang telah diupdate'
        this.pop_hapus = 'Hapus';
        this.pop_hapus_sukses = 'Barang telah dihapus'
      }
    });

    this.load_data();
  }

  ionViewDidEnter() {
    // this.load_data();
  }

  updateJudul() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = "UPDATE grup SET judul=? WHERE id_grup=?";
      db.executeSql(query, [this.judul, this.id_grup]).then(() => {
        console.log('data updated');
        this.showFooter = true;
        this.admobFree.banner.show();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  updateTanggal() {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = "UPDATE grup SET tanggal=? WHERE id_grup=?";
      db.executeSql(query, [this.tanggal.substr(0, 10), this.id_grup]).then(() => {
        console.log('data updated');
        this.admobFree.banner.show();
      }).catch(e => console.error(e));
    }).catch(e => console.error(e));
  }

  hideAdmob() {
    this.showFooter = false;
    this.admobFree.banner.hide();
  }

  load_data() {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        let query_grup: any = 'SELECT * FROM grup WHERE id_grup=?';
        db.executeSql(query_grup, [this.id_grup]).then((data) => {
          this.judul = data.rows.item(0).judul;
          this.tanggal = data.rows.item(0).tanggal;
        }).catch(e => console.error(e));

        let query: any = 'SELECT * FROM list WHERE id_grup=? ORDER BY st_check, nama_item';
        db.executeSql(query, [this.id_grup]).then((data) => {
          this.data = [];
          this.total = 0;

          if (data.rows.length > 0) {
            // this.empty = false;

            for (let i = 0; i < data.rows.length; i++) {
              this.data.push(data.rows.item(i));
              this.total += (data.rows.item(i).harga * data.rows.item(i).kuantiti)
            }

            this.loading = false;
          } else {
            // this.empty = true;
            this.loading = false;
          }

          console.info(this.data);
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    });
  }

  async popup_tambah() {
    this.admobFree.banner.hide();

    const alert = await this.alertController.create({
      header: this.pop_judul_tambah,
      inputs: [
        {
          name: 'nama_item',
          type: 'text',
          placeholder: this.pop_nama
        },
        {
          name: 'harga',
          type: 'number',
          placeholder: this.pop_harga
        },
        {
          name: 'kuantiti',
          type: 'number',
          placeholder: this.pop_kuantiti
        },
      ],
      buttons: [
        {
          text: this.pop_batal,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');

            this.admobFree.banner.show();
          }
        }, {
          text: this.pop_tambah,
          handler: val_input => {

            if (!val_input.harga) { val_input.harga = 0; }
            if (!val_input.kuantiti) { val_input.kuantiti = 0; }

            this.admobFree.banner.show();

            this.simpan(val_input.nama_item, val_input.harga, val_input.kuantiti)
          }
        }
      ]
    });
    await alert.present();
  }

  simpan(val_nama, val_harga, val_kuantiti) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      let query: any = 'INSERT INTO list ( id_grup, nama_item, harga, kuantiti, st_check ) VALUES ( ?, ?, ?, ?, ?)';
      db.executeSql(query, [this.id_grup, val_nama, val_harga, val_kuantiti, null]).then(async () => {
        console.log('new data inserted');

        this.load_data();
        // this.sorting();
        this.message_success(this.pop_tambah_sukses);
      }).catch(e => {
        console.error(e);
      });
    }).catch(e => console.error(e));
  }

  checking(val_id, val_checked) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      if (val_checked == true) {
        let query: any = "UPDATE list SET st_check=false WHERE id=?";
        db.executeSql(query, [val_id]).then(() => {
          console.log('item checked');
          this.load_data();
        }).catch(e => console.error(e));
      } else {
        let query: any = "UPDATE list SET st_check=true WHERE id=?";
        db.executeSql(query, [val_id]).then(() => {
          console.log('item checked');
          this.load_data();
        }).catch(e => console.error(e));
      }
    }).catch(e => console.error(e));
  }

  async edit(val_id, val_nama_item, val_harga, val_kuantiti) {
    this.admobFree.banner.hide();

    const alert = await this.alertController.create({
      header: this.pop_judul_update,
      inputs: [
        {
          name: 'nama_item',
          type: 'text',
          value: val_nama_item,
          placeholder: this.pop_nama
        },
        {
          name: 'harga',
          type: 'number',
          value: val_harga,
          placeholder: this.pop_harga
        },
        {
          name: 'kuantiti',
          type: 'number',
          value: val_kuantiti,
          placeholder: this.pop_kuantiti
        },
      ],
      buttons: [
        {
          text: this.pop_batal,
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');

            this.admobFree.banner.show();
          }
        },
        // {
        //   text: this.pop_hapus,
        //   handler: () => {

        //     this.sqlite.create({
        //       name: 'data.db',
        //       location: 'default'
        //     }).then((db: SQLiteObject) => {
        //       let query: any = 'DELETE FROM list WHERE id=?';
        //       db.executeSql(query, [val_id]).then(async () => {
        //         console.log('item deleted');

        //         this.message_delete(this.pop_hapus_sukses);
        //         this.load_data();
        //         this.admobFree.banner.show();
        //       }).catch(e => console.error(e));
        //     }).catch(e => console.error(e));
        //   }
        // },
        {
          text: this.pop_update,
          handler: val_input => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {

              if (!val_input.harga) { val_input.harga = 0; }
              if (!val_input.kuantiti) { val_input.kuantiti = 0; }

              let query: any = "UPDATE list SET nama_item=?, harga=?, kuantiti=? WHERE id=?";
              db.executeSql(query, [val_input.nama_item, val_input.harga, val_input.kuantiti, val_id]).then(() => {
                console.log('item updated');

                this.load_data();
                this.message_success(this.pop_update_sukses);
                this.admobFree.banner.show();
              }).catch(e => console.error(e));

            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  async hapus(val_id) {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Yakin untuk menghapus barang ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM list WHERE id=?';
              db.executeSql(query, [val_id]).then(async () => {
                console.log('item deleted');

                this.message_delete(this.pop_hapus_sukses);
                this.load_data();
                this.admobFree.banner.show();
              }).catch(e => console.error(e));
            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  tambah(val_id, val_kuantiti) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

      let qty = parseInt(val_kuantiti) + 1;

      let query: any = "UPDATE list SET kuantiti=? WHERE id=?";
      db.executeSql(query, [qty, val_id]).then(() => {
        console.log('item updated');

        this.load_data();

      }).catch(e => console.error(e));

    }).catch(e => console.error(e));
  }

  kurang(val_id, val_kuantiti) {
    if (val_kuantiti > 0) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        let qty = parseInt(val_kuantiti) - 1;

        let query: any = "UPDATE list SET kuantiti=? WHERE id=?";
        db.executeSql(query, [qty, val_id]).then(() => {
          console.log('item updated');

          this.load_data();

        }).catch(e => console.error(e));

      }).catch(e => console.error(e));
    }
  }

  async message_success(val_msg) {
    const toast = await this.toastController.create({
      message: val_msg,
      color: 'success',
      position: 'top',
      duration: 1600,
    });
    toast.present();
  }

  async message_info(val_msg) {
    const toast = await this.toastController.create({
      message: val_msg,
      color: 'tertiary',
      position: 'top',
      duration: 1600,
    });
    toast.present();
  }

  async message_delete(val_msg) {
    const toast = await this.toastController.create({
      message: val_msg,
      color: 'medium',
      position: 'top',
      duration: 1600,
    });
    toast.present();
  }

  async message_error(val_msg) {
    const toast = await this.toastController.create({
      message: JSON.stringify(val_msg.message),
      color: 'danger',
      position: 'top',
      duration: 1600,
    });
    toast.present();
  }

  async hapusGrup() {
    const alert = await this.alertController.create({
      header: 'Konfirmasi',
      message: 'Yakin untuk menghapus data ?',
      buttons: [
        {
          text: 'Batal',
          role: 'cancel',
        }, {
          text: 'Hapus',
          handler: () => {
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            }).then((db: SQLiteObject) => {
              let query: any = 'DELETE FROM grup WHERE id_grup=?';
              db.executeSql(query, [this.id_grup]).then(async () => {
                console.log('grup deleted');

                let query_items: any = 'DELETE FROM list WHERE id_grup=?';
                db.executeSql(query_items, [this.id_grup]).then(async () => {
                  console.log('items deleted');
                }).catch(e => console.error(e));

                const toast = await this.toastController.create({
                  message: 'Data telah terhapus.',
                  position: 'top',
                  color: 'medium',
                  duration: 1600,
                  buttons: [
                    {
                      text: 'x',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                      }
                    }
                  ]
                });
                toast.present();

                this.navCtrl.back();
              }).catch(e => console.error(e));

            }).catch(e => console.error(e));
          }
        }
      ]
    });
    await alert.present();
  }

  ionViewWillLeave() {
    if (this.data.length > 0) {
      this.sqlite.create({
        name: 'data.db',
        location: 'default'
      }).then((db: SQLiteObject) => {

        let query_cek_grup: any = 'SELECT * FROM grup WHERE id_grup=? ';
        db.executeSql(query_cek_grup, [this.id_grup]).then((data) => {

          if (data.rows.length == 0) {
            let query: any = 'INSERT INTO grup ( id_grup, judul, tanggal, total ) VALUES ( ?, ?, ?, ? )';
            db.executeSql(query, [this.id_grup, this.judul, this.tanggal.substr(0, 10), this.total]).then(async () => { console.log('new data inserted'); }).catch(e => { console.error(e); });
          } else {
            let query: any = "UPDATE grup SET total=? WHERE id_grup=?";
            db.executeSql(query, [this.total, this.id_grup]).then(() => { console.log('data updated'); }).catch(e => console.error(e));
          }
        }).catch(e => console.error(e));
      }).catch(e => console.error(e));
    }
  }

}
