import { Component, OnInit, ViewEncapsulation } from '@angular/core';
// import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  private bahasa = new Subject<any>();
  pubBahasa(data: any) { this.bahasa.next(data); }
  obvBahasa(): Subject<any> { return this.bahasa; }

  appPages = [
    {
      title: 'Home',
      url: '/app/tabs/home',
      icon: 'calendar'
    },
  ];
  loggedIn = false;
  dark = false;

  constructor(
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public storage: Storage,
    public translate: TranslateService,
  ) {
    this.translate.setDefaultLang('en');
    this.initializeApp();
  }

  async ngOnInit() {
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.obvBahasa().subscribe((val) => {
        this.storage.set('bahasa', val.bahasa).then(() => {
          this.translate.use(val.bahasa);
        });
      });

      this.storage.get('bahasa').then((val) => {
        if (val == null) {
          this.translate.use('en');
        } else {
          this.translate.use(val);
        }
      });


      this.statusBar.backgroundColorByHexString('123456');
      // this.statusBar.styleLightContent();
      this.splashScreen.hide();

      // this.translate.use('en');
    });
  }

  ionViewWIllEnter() {
    this.platform.ready().then(() => {
      // this.translate.use('en');
    });
  }
}
